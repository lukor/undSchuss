#!/bin/bash
function usage() {
    cat <<EOF
Usage: $0 [options] [action]

        -h| --help                          Display this help
        -p <port>| --port=<port>            Specify the port number to run on
        -d <path>| --directory=<path>       Specify the path to run in
        -m <mode>| --mode=<mode>            Specify the mode (fs/...)

        ls| list                            List all current server instances
        stop <server>| kill <server>        Kill a server instance
EOF
}
if [ "$(id -u)" != "0" ]; then
    echo This server needs root privilieges to run. Aborting. >&2
    exit 1
fi

mkdir /tmp/undSchuss 2>/dev/null
mkdir /tmp/undSchuss/serverInfo 2>/dev/null
mkdir /tmp/undSchuss/$$
pipe=/tmp/undSchuss/$$
serverInfo=/tmp/undSchuss/serverInfo/$$
pid=$$
stdout=`readlink -f /proc/$$/fd/1`
stderr=`readlink -f /dev/stderr`

function cleanUp() {
    rm -f $pipe/pipe$1
    rmdir $pipe &> /dev/null
    rm $serverInfo &> /dev/null
    rmdir /tmp/undSchuss/serverInfo &> /dev/null
    rmdir /tmp/undSchuss &> /dev/null
}

function handleRequest() {
    trap "cleanUp $! > /dev/pts/2; exit 0" SIGINT SIGTERM
    local forked
    while read line; do
        if [[ ! $forked ]]; then
            forked=1
            ( serverThread >&2 & )
        fi
        line=$(echo "$line"|tr -d '\r')
        case "$line" in
            GET\ *\ HTTP/1.1) REQUESTPATH=`echo $line|awk '{print $2}'` ;;
            "") break ;;
            *) ;;
        esac
    done < <(cat $1)
    if [[ "fs" == $MODE ]]; then
        fsHandler $REQUESTPATH
    fi
}

function fsNotFound() {
        echo HTTP/1.1 404 NOT FOUND
        echo
        echo Resource \'"$1"\' not found
}
function fsHandler() {
    if [[ ! -e ".$1" ]]; then
        fsNotFound "$1"
        return
    fi
    if [[ $(realpath "--relative-to=$DIR" ".$1") == .. ]]; then
        fsNotFound "$1"
        return
    else
        r=".$1"
        if [[ -d ".$1" ]]; then
            ls ".$1"/index.* &>/dev/null || (fsNotFound "$1" && return)
            r=$(ls ".$1"/index.*|head -n 1)
        fi
        echo HTTP/1.1 200 OK
        echo Content-Type: $(file -i "$r"|awk '{print $2}') charset=UTF-8
        echo Content-Length: $(wc -c "$r" |awk '{print $1}')
        echo
        cat "$r"
    fi
}

args=`getopt -o m:d:p:h --long mode:,directory:,port:,help -n undSchuss -- "$@"`
eval set -- "$args"
PORT=80
DIR=$(pwd)
MODE=fs
while :; do
    case "$1" in
        -p|--port) shift; PORT=$1; shift ;;
        -d|--directory) shift; DIR=$1; shift ;;
        -m|--mode) shift
                  if grep -w $(echo "$1"|sed 's/.* .*/no/') <(echo "fs othermodeshere") >/dev/null; then
                      MODE=$1
                      shift
                  else
                      echo Invalid mode \'"$1"\' >&2
                      usage
                      exit 1
                  fi
                  ;;
        -h|--help) shift; usage; exit 1 ;;
        --) shift; break ;;
        *) shift; echo no ;;
    esac
done
for extraArg in "$@"; do
    case "$extraArg" in
        ls|list) ls /tmp/undSchuss/serverInfo/* &>/dev/null && cat /tmp/undSchuss/serverInfo/* || echo No server instance running; cleanUp 0; exit 0 ;;
        stop|kill) kill=1 ;;
        *) if [[ "$kill" ]]; then
             if ls /tmp/undSchuss/serverInfo/$extraArg &> /dev/null; then
                 ps auxf|grep -v grep|grep  "nc -l $(cat /tmp/undSchuss/serverInfo/$extraArg|awk '{print $5}')" -B 4|awk '{print $2}'|xargs kill
                 cleanUp 0
                 exit 0
             else
                 echo Server instance \'$extraArg\' not found. Aborting. >&2
                 cleanUp 0
                 exit 1
             fi
           else
               echo Invalid argument: $extraArg >&2
               usage
               cleanUp 0
               exit 1
           fi ;;
    esac
done
if lsof -i :$PORT >/dev/null; then
    echo Port already in use. Aborting. >&2
    cleanUp 0
    exit 1
fi
echo Running on port $PORT in $MODE mode \($DIR\) >&2
cd $DIR
echo Server $pid on Port $PORT in $DIR in $MODE mode > $serverInfo

function serverThread() {
    pid=$!
    if [[ ! -p $pipe/pipe$pid ]]; then
        mkfifo $pipe/pipe$pid
    else
        echo "Pipe error" >$stderr
        exit 1
    fi
    handleRequest $pipe/pipe$pid |nc -l $PORT > $pipe/pipe$pid
    rm -f $pipe/pipe$pid
}
( serverThread 1 & )
